# LabelStudioHelper

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/LabelStudioHelper.jl/dev)
[![Build Status](https://github.com/mcamp/LabelStudioHelper.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/mcamp/LabelStudioHelper.jl/actions/workflows/CI.yml?query=branch%3Amain)
[![Build Status](https://github.com/mcamp/LabelStudioHelper.jl/badges/main/pipeline.svg)](https://github.com/mcamp/LabelStudioHelper.jl/pipelines)
[![Coverage](https://github.com/mcamp/LabelStudioHelper.jl/badges/main/coverage.svg)](https://github.com/mcamp/LabelStudioHelper.jl/commits/main)
[![Coverage](https://codecov.io/gh/mcamp/LabelStudioHelper.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/mcamp/LabelStudioHelper.jl)
