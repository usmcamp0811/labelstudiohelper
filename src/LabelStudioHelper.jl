module LabelStudioHelper
using JSON
using Images, ImageFiltering, Plots
using Dates
using Base.Iterators: take
using AWSS3
using AWS: global_aws_config, AWSConfig
using Minio


mutable struct InputStream
    data::String
    i::Int

    InputStream(data) = new(data, 0)
end

function read!(stream::InputStream, size::Int)
  out = stream.data[stream.i+1:stream.i+size]
  stream.i += size
  return parse(Int, out, base=2)
end

access_bit(data::Vector{UInt8}, num::Int) = (data[num ÷ 8 + 1] & (1 << (7 - num % 8))) >> (7 - num % 8)

bytes2bit(data::Vector{UInt8}) = join([string(access_bit(data, i)) for i in 0:length(data) * 8 - 1])

"""
Reconstruct the Image Mask from the RLE in a Label Studio Image result
"""
function rle_to_mask(rle::Vector{UInt8}, height::Int, width::Int)
  rle_input = InputStream(bytes2bit(rle))
  num = read!(rle_input,32)
  word_size = read!(rle_input,5) + 1
  rle_sizes = [read!(rle_input,4) + 1 for _ in 1:4]

  i = 0
  out = zeros(UInt8,num)
  while i < num
    pre = rle_input
    x = read!(rle_input,1)
    s = rle_sizes[read!(rle_input,2)+1]
    j = i + 1 + read!(rle_input,s)
    if x != 0
      val = read!(rle_input,word_size)
      out[i+1:j] .= val
      i = j
    else
      while i < j
        val = read!(rle_input,word_size)
        out[i+1] = val
        i += 1
      end
    end
  end
  # properly reshape the image
  image = transpose(reverse(reshape(Float32.(out), (4, width, height))[1,:,:], dims=1)) 
  # flip horizontally because the above made it mirrored
  image[:, end:-1:1]
end

rle_to_mask(rle::Vector{Int},height::Int,width::Int) = rle_to_mask(UInt8.(rle),height,width)
 
"""
A Struct that provides both the path to the data and the mask/label
"""
struct LabeledData
  id::Int
  inner_id::Int
  project::Int
  updated_at::DateTime
  data::Dict{String,Any}
  mask::Union{Vector{Int},Nothing}
  labels::Vector{Any}
  format::String
  height::Int
  width::Int
  LabeledData(id::Int,inner_id::Int,project::Int,updated_at::DateTime,data::Dict{String,Any},result::Dict{String,Any}) = new(
    id,
    inner_id,
    project,
    updated_at,
    data,
    result["value"]["rle"],
    result["value"]["brushlabels"],
    result["value"]["format"],
    result["original_height"],
    result["original_width"],
  )
  LabeledData(id::Int,inner_id::Int,project::Int,updated_at::DateTime,data) = new(
    id,
    inner_id,
    project,
    updated_at,
    data,
    nothing,
    [],
    "None",
    0,
    0,
  )
end


"""
Parse a single task/object in a Label Studio JSON Array
"""
function parseTask(task::Dict{String,Any})
  id = task["id"]
  inner_id = task["inner_id"]
  project = task["project"]
  data = task["data"]
  annotations = task["annotations"]
  updated_at = DateTime(task["updated_at"][1:19], dateformat"y-m-dTH:M:S")
  out = []
  for annotation in annotations
    for result in annotation["result"] 
      push!(out,LabeledData(id,inner_id,project,updated_at,data,result))
    end
  end
  if length(out) == 0
    out = [
      LabeledData(id,inner_id,project,updated_at,data)
    ]
  end
  return out
end


"""
Read a Label Studio JSON into an Array of LabeledData
"""
parsejson(path::String) = vcat(parseTask.(JSON.parsefile(path))...)


function render_mask(data::LabeledData; render_skips=false)
  if data.mask == nothing 
    if render_skips == true
      img_size = size(render_data(data))
      return image_view(zeros(img_size))
    end
  else
    return image_view(rle_to_mask(UInt8.(data.mask),data.height,data.width))
  end
end

function render_data(data::LabeledData; s3creds=MinioConfig("https://s3-api.lan.aicampground.com", AWSCredentials(;profile="campground"), region="us-east-1"))
  load(IOBuffer(s3_get_from_url(data.data["image"],s3creds)))
end

function save_mask(data::LabeledData; render_skips=false, mask_save_dir="./labels")
  mask = render_mask(data; render_skips=render_skips)
  filename = joinpath(mask_save_dir,getLabelName(data))
  if mask ≠ nothing
    save(filename,map(clamp01nan,mask))
  end
  # mask
  println("Saved: $(filename)")
end

getFilename(data::LabeledData) = basename(last(first(data.data)))

function getLabelName(data::LabeledData) 
  mask = "-LABEL-project-$(data.project)-id-$(data.id)-inner_id-$(data.inner_id)."
  filename = getFilename(data)
  parts = split(filename, ".")
  new_filename = parts[1] * mask * parts[2]
end

function image_view(img)
  if length(size(img)) == 2 #Gray sacle
    img = colorview(Gray, reshape(img, size(img, 1), size(img, 2)))
  elseif length(size(img)) == 3 #RGB
    if size(img)[3] == 3
      img = colorview(RGB, permutedims(img, (3, 1, 2)))
    else
      img = colorview(Gray, reshape(img, size(img, 1), size(img, 2)))
    end
  else
    img = reshape(img, size(img,1),size(img,2),size(img,3))
    image_view(img)
  end
end

function s3_get_from_url(s3_url::String, s3config)
    global_aws_config(s3config)
    # Remove the "s3://" prefix and split the string into bucket name and file path
    bucket_and_path = replace(s3_url, "s3://" => "")
    bucket_name, file_path = split(bucket_and_path, "/", limit=2)

    # Perform the s3_get operation
    data = s3_get(bucket_name, file_path)
    return data
end

# s3creds = AWSCredentials(;profile="campground")

# label_studio = parsejson("project-1-at-2023-11-30-18-48-cb3a982e.json")
# label_studio = parsejson("project-1-at-2023-12-03-19-15-b837df41.json")
#
# global_aws_config(MinioConfig("https://s3-api.lan.aicampground.com", s3creds; region="us-east-1"))
export image_view, save_mask, render_data, render_mask, parseTask, parsejson, getLabelName, getFilename
end
