using LabelStudioHelper
using Documenter

DocMeta.setdocmeta!(LabelStudioHelper, :DocTestSetup, :(using LabelStudioHelper); recursive=true)

makedocs(;
    modules=[LabelStudioHelper],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/LabelStudioHelper.jl/blob/{commit}{path}#{line}",
    sitename="LabelStudioHelper.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/LabelStudioHelper.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
